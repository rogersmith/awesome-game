package Manage;

import Entities.Entity;
import Tiles.Tile;

public class GameCamera {

    // Data Members
    private Handler handler;
    private float xOffset;
    private float yOffset;

    // Ctor

    public GameCamera(Handler handler, float xOffset, float yOffset){
        this.handler = handler;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
    }

    // Other Methods

    public void checkBlankSpace(){
        if(this.getOffsetX() < 0){
            this.xOffset = 0;
        } else if(this.getOffsetX() > handler.getWorld().width() * Tile.TILE_WIDTH - handler.getWidth()) {
            this.xOffset = handler.getWorld().width() * Tile.TILE_WIDTH - handler.getWidth();
        }

        if(this.getOffsetY() < 0){
            this.yOffset = 0;
        } else if(this.getOffsetY() > handler.getWorld().height() * Tile.TILE_HEIGHT - handler.getHeight()) {
            this.yOffset = handler.getWorld().height() * Tile.TILE_HEIGHT - handler.getHeight();
        }
    }

    public void CenterOnEntity(Entity entity){
        this.xOffset = entity.entityX() - this.handler.getWidth() / 2 + entity.entityWidth()/2;
        this.yOffset = entity.entityY() - this.handler.getHeight() / 4;
        this.checkBlankSpace();
    }

    public void move(float amX, float amY){
        this.xOffset += amX;
        this.yOffset += amY;
        this.checkBlankSpace();
    }

    // Access Mehtods

    public float getOffsetX() {
        return this.xOffset;
    }

    public void setOffsetX(float xOffset) { this.xOffset = xOffset;}

    public float getOffsetY() {
        return this.yOffset;
    }

    public void setOffsetY(float yOffset) { this.yOffset = yOffset;}
}
