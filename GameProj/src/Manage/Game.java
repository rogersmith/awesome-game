package Manage;

import Gfx.Assets;
import Gfx.Display;
import Input.*;
import States.*;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Game implements Runnable {
    private int width;
    private int height;
    private String title;
    private boolean isRunning = false;
    private Display display;

    private Thread thread;
    private BufferStrategy bufferStrategy;
    private Graphics g;
    private GameCamera gameCamera;
    private KeyManager keyManager;
    private MouseManager mouseManager;
    private Handler handler;

    public State gameState;
    public State menuState;
    public State settingsState;

    public Game(String title, int width, int height) {
        this.height = height;
        this.width = width;
        this.title = title;
        keyManager = new KeyManager();
        mouseManager = new MouseManager();
    }

    private void init(){
        this.display = new Display(this.title, this.width, this.height);
        this.display.jFrame().addKeyListener(this.keyManager);
        this.display.jFrame().addMouseListener(this.mouseManager);
        this.display.jFrame().addMouseMotionListener(this.mouseManager);
        this.display.canvas().addMouseListener(this.mouseManager);
        this.display.canvas().addMouseMotionListener(this.mouseManager);
        Assets.init();
        handler = new Handler(this);
        gameCamera = new GameCamera(handler,0,0);

        gameState = new GameState(this.handler);
        menuState = new MenuState(this.handler);
        settingsState = new SettingsState(this.handler);
        State.setState(menuState);
    }

    private void tick(){
        this.keyManager.tick();

        if(State.getState() != null){
            State.getState().tick();
        }
    }

    private void render(){
        bufferStrategy = display.canvas().getBufferStrategy();
        if(bufferStrategy == null){
            display.canvas().createBufferStrategy(3);
            return;
        }
        g = bufferStrategy.getDrawGraphics();
        //this.g.clearRect(0,0, this.width, this.height);
        g.drawImage(Assets.background,0,0, width, height, null);

        if(State.getState() != null){
            State.getState().render(g);
        }

        bufferStrategy.show();
        g.dispose();
    }

    public void run(){
        init();

        int fps = 60;
        double timePerTick = 1000000000 / fps;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();

        while(this.isRunning){
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            lastTime = now;

            if(delta >= 1) {
                tick();
                render();
                delta--;
            }
        }

        stop();
    }

    public synchronized void start(){
        if(!this.isRunning) {
            this.isRunning = true;
            this.thread = new Thread(this);
            this.thread.start();
        }
    }

    public synchronized void stop(){
        if(this.isRunning){
            this.isRunning = false;
            try {
                this.thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public int getWidth(){
        return (this.width);
    }

    public int getHeight(){
        return (this.height);
    }

    public KeyManager getKeyManager() {
        return (this.keyManager);
    }

    public MouseManager getMouseManager() {
        return (this.mouseManager);
    }

    public GameCamera getGameCamera() {
        return this.gameCamera;
    }
}
