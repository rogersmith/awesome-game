package Manage;

import Entities.*;
import Tiles.Tile;
import java.awt.*;

public class World {
    private Handler handler;
    private int width;
    private int height;
    private int xSpawn;
    private int ySpawn;
    private int [][] tiles;
    private EntityManager entityManager;

    public World(Handler handler, String path) {
        this.handler = handler;
        this.entityManager = new EntityManager(handler, new Player(handler,0,0));
        loadWorld(path);
        this.entityManager.getPlayer().positionPlayer(this.xSpawn,this.ySpawn);
        this.entityManager.addEntity(new Enemy(this.handler,100,100));
    }

    public void tick() {
        entityManager.tick();
        for(int i = 0; i < width; i++) {
            for (int j = 1; j < height; j++) {
                checkIfNothingAbove(i,j);
            }
        }
    }

    public void render(Graphics g) {
        int xStart = (int) Math.max(0, (handler.getGameCamera().getOffsetX() / Tile.TILE_WIDTH));
        int xEnd = (int) Math.min(width, ((handler.getGameCamera().getOffsetX() + handler.getWidth()) / Tile.TILE_WIDTH) + 4);
        int yStart = (int) Math.max(0, (handler.getGameCamera().getOffsetY() / Tile.TILE_HEIGHT));
        int yEnd = (int) Math.min(height, ((handler.getGameCamera().getOffsetY() + handler.getHeight()) / Tile.TILE_HEIGHT) + 4);
        int miniMapWidth = (int)((Tile.MINI_TILE_WIDTH * width)/1.2);
        int miniMapX = handler.getWidth() - miniMapWidth - 17;
        int miniMapHeight = (int)((Tile.MINI_TILE_HEIGHT * height)/1.2);

        g.setColor(Color.GRAY);
        g.fillRect(miniMapX - 5 ,10, miniMapWidth + 12,miniMapHeight + 11);
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(miniMapX,15, miniMapWidth + 2, miniMapHeight + 1);

        for(int i = yStart; i < yEnd; i++){
            for(int j = xStart; j < xEnd; j++){
                getTile(j,i).render(g,(int)(j * Tile.TILE_WIDTH - this.handler.getGameCamera().getOffsetX()), (int)(i * Tile.TILE_HEIGHT - this.handler.getGameCamera().getOffsetY()));
            }
        }

        for(int i = 0; i < height; i++) {
            for(int j = 0; j < width; j++) {
                if(tiles[j][i] > 0) {
                    getTile(j,i).miniMapRender(g,j * Tile.TILE_WIDTH,i * Tile.TILE_HEIGHT, miniMapX);
                }
            }
        }

        entityManager.render(g);
    }

    public Tile getTile(int x, int y){
        if(x < 0 || y < 0 || x >= width || y >= height){
            return Tile.grass1;
        }

        if(Tile.tiles[this.tiles[x][y]] != null) {
            return Tile.tiles[this.tiles[x][y]];
        }
        return Tile.tiles[2];
    }

    public void loadWorld(String path) {
        String file = Utils.loadFileString(path);
        String[] tokens = file.split("\\s+");
        width = Integer.parseInt(tokens[0]);
        height = Integer.parseInt(tokens[1]);
        xSpawn = Integer.parseInt(tokens[2]);
        ySpawn = Integer.parseInt(tokens[3]);

        tiles = new int[width][height];
        for(int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                tiles[i][j] = Integer.parseInt(tokens[ (i + j * width) + 4]);
            }
        }
    }

    public int width(){
        return this.width;
    }

    public int height(){
        return this.height;
    }

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    public int [][] getTiles() {
        return (this.tiles);
    }

    public void checkIfNothingAbove(int x, int y) {
        if (tiles[x][y] > 3) {
            if (tiles[x][y - 1] == 0) {
                tiles[x][y] = 3;
            }
        }
    }
}
