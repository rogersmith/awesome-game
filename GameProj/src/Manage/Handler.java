package Manage;

import Input.*;

public class Handler {
    private Game game;
    private World world;

    public Handler(Game game){
        this.game = game;
    }

    public GameCamera getGameCamera(){
        return this.game.getGameCamera();
    }

    public KeyManager getKeyManager(){
        return (this.game.getKeyManager());
    }

    public MouseManager getMouseManager() {
        return (this.game.getMouseManager());
    }

    public int getWidth(){
        return this.game.getWidth();
    }

    public int getHeight(){
        return this.game.getHeight();
    }

    public Game getGame() {
        return this.game;
    }

    public World getWorld() {
        return this.world;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void setWorld(World world) {
        this.world = world;
    }
}
