package States;

import Manage.Handler;
import java.awt.*;

public class MenuState extends State {

    public MenuState(Handler handler){
        super(handler);
    }

    @Override
    public void tick() {
        if(handler.getMouseManager().isLeftPressed()){
            State.setState(handler.getGame().gameState);
        }
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.GRAY);
        g.fillRect(0,0,handler.getWidth(),handler.getHeight());
    }
}
