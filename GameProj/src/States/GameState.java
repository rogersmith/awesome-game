package States;

import Entities.Player;
import Manage.Handler;
import Manage.World;

import java.awt.*;

public class GameState extends State {
    private Player player;
    private World world;

    public GameState(Handler handler){
        super(handler);
        this.world = new World(handler,"res\\Worlds\\World1.txt");
        handler.setWorld(this.world);
    }

    @Override
    public void tick() {
        this.world.tick();
    }

    @Override
    public void render(Graphics g) {
        this.world.render(g);
    }
}
