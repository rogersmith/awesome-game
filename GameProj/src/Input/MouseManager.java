package Input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.peer.MouseInfoPeer;

public class MouseManager implements MouseListener, MouseMotionListener {

    private boolean leftPressed;
    private boolean rightPressed;
    private int mouseX;
    private int mouseY;

    public MouseManager(){

    }

    public boolean isLeftPressed() {
        return (this.leftPressed);
    }

    public boolean isRightPressed() {
        return (this.rightPressed);
    }

    public int getMouseX() {
        return (this.mouseX);
    }

    public int getMouseY() {
        return (this.mouseY);
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseEvent.BUTTON1){
            leftPressed = true;
        } else if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
            rightPressed = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseEvent.BUTTON1){
            leftPressed = false;
        } else if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
            rightPressed = false;
        }
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        mouseX = mouseEvent.getX();
        mouseY = mouseEvent.getY();
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }
}
