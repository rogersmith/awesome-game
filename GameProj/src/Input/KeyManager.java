package Input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener {
    private boolean[] keys;
    public boolean up,right,left,bAttack,hAttack,dash,up2,right2,left2,bAttack2,dash2;

    public KeyManager(){
        this.keys = new boolean[256];
    }

    public void tick(){
        up = keys[KeyEvent.VK_UP];
        left = keys[KeyEvent.VK_LEFT];
        right = keys[KeyEvent.VK_RIGHT];
        bAttack = keys[KeyEvent.VK_K];
        hAttack = keys[KeyEvent.VK_T];
        dash = keys[KeyEvent.VK_L];
        up2 = keys[KeyEvent.VK_W];
        left2 = keys[KeyEvent.VK_A];
        right2 = keys[KeyEvent.VK_D];
        bAttack2 = keys[KeyEvent.VK_SPACE];
        dash2 = keys[KeyEvent.VK_Q];
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        this.keys[keyEvent.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        this.keys[keyEvent.getKeyCode()] = false;
    }
}
