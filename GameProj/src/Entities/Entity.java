package Entities;

import Manage.Handler;
import java.awt.*;

public abstract class Entity {
    public static final int DEFAULT_HEALTH = 100;

    public Handler handler;
    protected float x;
    protected float y;
    protected int healthPoints;
    protected int width;
    protected int height;
    protected boolean active;
    protected boolean stagered;
    protected Rectangle bounds;

    public Entity(Handler handler, float x, float y, int width, int height){
        this.handler = handler;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.healthPoints = DEFAULT_HEALTH;
        this.active = true;
        this.bounds = new Rectangle(0,0,this.width,this.height);
    }

    public int healthPoints() {
        return (this.healthPoints);
    }

    public boolean isActive() {
        return (this.active);
    }

    public int entityWidth(){
        return (this.width);
    }

    public int entityHeight(){
        return (this.height);
    }

    public float entityX(){
        return (this.x);
    }

    public float entityY(){
        return (this.y);
    }

    public Rectangle collisionBounds(float xOffset, float yOffset) {
        return new Rectangle((int)(this.x + this.bounds.x + xOffset),
                             (int)(this.y + this.bounds.y + yOffset),
                             bounds.width,
                             bounds.height);
    }

    public abstract void die();

    public void hurt(int damage) {
        this.healthPoints -= damage;
        stagered = true;
        if (this.healthPoints <= 0){
            this.active = false;
            die();
        }
    }

    public abstract void tick();

    public abstract void render(Graphics g);
}
