package Entities;

import Tiles.Tile;
import Manage.Handler;

public abstract class Creature extends Entity {
    public static final float DEFAULT_SPEED = 5.0f;
    public static final int DEFAULT_WIDTH = 100;
    public static final int DEFAULT_HEIGHT = 74;

    protected boolean directionRight;
    protected float creatureSpeed;
    protected float xVector;
    protected float yVector;

    public Creature(Handler handler, float fX, float fY, int nWidth, int nHeight) {
        super(handler, fX, fY, nWidth, nHeight);
        this.creatureSpeed = DEFAULT_SPEED;
        this.directionRight = true;
    }

    public float xVector() {
        return this.xVector;
    }

    public float yVector() {
        return this.yVector;
    }

    public float creatureSpeed() {
        return this.creatureSpeed;
    }

    public void move(){
        moveX();
        moveY();
    }

    public void moveX(){
        if(this.xVector() > 0){
            int tx = (int) (this.entityX() + this.xVector() + bounds.x + bounds.width) / Tile.TILE_WIDTH;
            directionRight = true;

            if(!collisionWithTile(tx,(int)(this.entityY() + bounds.y) / Tile.TILE_HEIGHT) &&
               !collisionWithTile(tx,(int)(this.entityY() + bounds.y + bounds.height) / Tile.TILE_HEIGHT)){
                this.x = this.entityX() + this.xVector();
            } else {
                this.x = tx * Tile.TILE_WIDTH - bounds.x - bounds.width;
            }
        } else if(this.xVector() < 0) {
            int tx = (int) (this.entityX() + this.xVector() + bounds.x) / Tile.TILE_WIDTH ;
            directionRight = false;

            if(!collisionWithTile(tx,(int)(this.entityY() + bounds.y) / Tile.TILE_HEIGHT) &&
               !collisionWithTile(tx,(int)(this.entityY() + bounds.y + bounds.height) / Tile.TILE_HEIGHT)){
                this.x = this.entityX() + this.xVector();
                if(this.x <= -bounds.width) {
                    this.x = -bounds.width;
                }
            } else {
                this.x = (tx + 1) * Tile.TILE_WIDTH - bounds.x;
            }
        }
    }

    public void moveY(){
        if(this.yVector() < 0){
            int ty = (int) (this.entityY() + this.yVector() + bounds.y) / Tile.TILE_HEIGHT;
            if(!collisionWithTile((int)(this.entityX() + bounds.x + 1) / Tile.TILE_WIDTH, ty) &&
               !collisionWithTile((int)(this.entityX() + bounds.x + bounds.width - 1) / Tile.TILE_WIDTH, ty)){
                yVector = this.yVector();
            } else {
                yVector = 0;
            }
            this.y = this.entityY() + this.yVector();
        } else if(this.yVector() > 0) {
            int ty = (int) (this.entityY() + this.yVector() + bounds.y + bounds.height) / Tile.TILE_HEIGHT;

            if(!collisionWithTile((int)(this.entityX() + bounds.x + 1) / Tile.TILE_WIDTH, ty) &&
               !collisionWithTile((int)(this.entityX() + bounds.x + bounds.width - 1) / Tile.TILE_WIDTH, ty)){
                this.y = this.entityY() + this.yVector();
            }
        }
    }

    protected boolean collisionWithTile(int x, int y){
        return (handler.getWorld().getTile(x,y)).isSolid();
    }
}
