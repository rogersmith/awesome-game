package Entities;

import Manage.Handler;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class EntityManager {
    private Handler handler;
    private Player player;
    private ArrayList<Entity> entities;

    public EntityManager(Handler handler, Player player){
        this.handler = handler;
        this.player = player;
        this.entities = new ArrayList<>();
        addEntity(this.player);
    }

    public void addEntity(Entity entityToAdd){
        entities.add(entityToAdd);
    }

    public void tick() {
        Iterator<Entity> entitiesIterator = this.entities.iterator();
        while(entitiesIterator.hasNext()) {
            Entity entity = entitiesIterator.next();
            entity.tick();
            if(!entity.isActive()) {
                entitiesIterator.remove();
            }
        }
    }

    public void render(Graphics g) {
        for(Entity entity : this.entities){
            entity.render(g);
        }
    }

    public ArrayList<Entity> getEntities() {
        return (this.entities);
    }

    public Player getPlayer() {
        return this.player;
    }
}
