package Entities;

import Gfx.Animation;
import Gfx.Assets;
import Tiles.Tile;
import Manage.Handler;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Player extends Creature {
    final long ATTACK_COOLDOWN = 800;
    final long DASH_COOLDOWN = 3000;

    private Animation lookingLeftAnimation;
    private Animation lookingRightAnimation;
    private Animation walkingLeftAnimation;
    private Animation walkingRightAnimation;
    private Animation attackingLeftAnimation;
    private Animation attackingRightAnimation;
    private Animation dashingLeftAnimation;
    private Animation dashingRightAnimation;
    private Animation stageredLeftAnimation;
    private Animation stageredRightAnimation;
    private Rectangle hitBox = new Rectangle();
    private boolean attacking;
    private boolean dashing;
    private long lastDashTimer;
    private long dashTimer;
    private long lastAttackTimer;
    private long attackTimer;
    private long stagerTimer;

    public Player(Handler handler, float x, float y) {
        super(handler, x, y, Creature.DEFAULT_WIDTH, Creature.DEFAULT_HEIGHT);

        this.bounds.x = 35;
        this.bounds.y = 34;
        this.bounds.width = 30;
        this.bounds.height = 36;

        lookingLeftAnimation = new Animation(400, Assets.playerLookingLeft);
        lookingRightAnimation = new Animation(400, Assets.playerLookingRight);
        walkingLeftAnimation = new Animation(100, Assets.playerWalkingLeft);
        walkingRightAnimation = new Animation(100, Assets.playerWalkingRight);
        attackingLeftAnimation = new Animation(100, Assets.playerAttackingLeft);
        attackingRightAnimation = new Animation(100, Assets.playerAttackingRight);
        dashingLeftAnimation = new Animation(100, Assets.playerDashingLeft);
        dashingRightAnimation = new Animation(100, Assets.playerDashingRight);
        stageredLeftAnimation = new Animation(100, Assets.playerStageredLeft);
        stageredRightAnimation = new Animation(100, Assets.playerStageredRight);

        this.stagered = false;
        this.attacking = false;
        this.dashing = false;
        this.attackTimer = this.ATTACK_COOLDOWN;
        this.dashTimer = this.DASH_COOLDOWN;
        this.stagerTimer = 0;
    }

    @Override
    public void tick() {
        if (stagered) {
            stager();
        } else {
            if (attacking) {
                if (directionRight) {
                    attackingRightAnimation.tick();
                    if (attackingRightAnimation.frameIndex() == 3) {
                        attacking = false;
                    }
                } else {
                    attackingLeftAnimation.tick();
                    if (attackingLeftAnimation.frameIndex() == 3) {
                        attacking = false;
                    }
                }
            }
            if (dashing) {
                creatureSpeed = 25.0f;
                if (dashTimer >= 150) {
                    dashing = false;
                }
            } else {
                creatureSpeed = 5.0f;
            }
            handleInput();
            move();
            checkDash();
            checkAttacks();
        }
        walkingLeftAnimation.tick();
        walkingRightAnimation.tick();
        lookingRightAnimation.tick();
        lookingLeftAnimation.tick();
        stageredRightAnimation.tick();
        stageredLeftAnimation.tick();
        dashingRightAnimation.tick();
        dashingLeftAnimation.tick();
        handler.getGameCamera().CenterOnEntity(this);
    }

    @Override
    public void render(Graphics g) {
        drawHpBar(g);
        drawDashCooldown(g);
        drawToMinimap(g);
        g.drawImage(this.getCurrentAnimationFrame(), (int) (x - handler.getGameCamera().getOffsetX()), (int) (y - handler.getGameCamera().getOffsetY()), width, height, null);
    }

    @Override
    public void die() {
        System.out.println("Player 1 lose");
    }

    public void checkDash() {
        dashTimer += System.currentTimeMillis() - lastDashTimer;
        lastDashTimer = System.currentTimeMillis();
        if (dashTimer >= DASH_COOLDOWN) {
            if (handler.getKeyManager().dash) {
                if (handler.getKeyManager().left || handler.getKeyManager().right) {
                    dashing = true;
                    dashTimer = 0;
                }
            }
        }
    }

    public void checkAttacks()  {
        attackTimer += System.currentTimeMillis() - lastAttackTimer;
        lastAttackTimer = System.currentTimeMillis();
        Rectangle collisionBox = this.collisionBounds(0, 0);
        int hitBoxSize = 30;
        hitBox.width = hitBoxSize;
        hitBox.height = hitBoxSize;
        hitBox.y = collisionBox.y + collisionBox.height / 2 - hitBoxSize / 2;

        if (directionRight) {
            hitBox.x = collisionBox.x + collisionBox.width;
        } else {
            hitBox.x = collisionBox.x - hitBoxSize;
        }

        if (attacking) {
            for (Entity entity : handler.getWorld().getEntityManager().getEntities()) {
                if (entity.collisionBounds(0, 0).intersects(hitBox) && !entity.equals(this)) {
                    entity.hurt(1);
                }
            }
            for (int i = 0; i < handler.getWorld().width(); i++) {
                for (int j = 0; j < handler.getWorld().height(); j++) {
                    Rectangle tile = new Rectangle(i * Tile.TILE_WIDTH, j * Tile.TILE_HEIGHT, Tile.TILE_WIDTH, Tile.TILE_HEIGHT);
                    if(tile.intersects(hitBox)) {
                        if(handler.getWorld().getTile(i,j).getId() != 0) {
                            handler.getWorld().getTiles()[i][j] = 0;
                        }
                    }
                }
            }
        }

        if (attackTimer >= ATTACK_COOLDOWN) {
            if (handler.getKeyManager().bAttack) {
                attacking = true;
                attackTimer = 0;
            }
        }
    }

    public void stager() {
        stagerTimer++;
        if (stagerTimer >= 2) {
            stagered = false;
            stagerTimer = 0;
        }
    }

    public void handleInput() {
        this.xVector = 0;
        int ty = (int) (this.entityY() + this.yVector() + bounds.y + bounds.height) / Tile.TILE_HEIGHT;

        if (this.handler.getKeyManager().right) {
            this.xVector = this.creatureSpeed();
        }
        if (this.handler.getKeyManager().left) {
            this.xVector = -this.creatureSpeed();
        }
        if (collisionWithTile((int) (this.entityX() + bounds.x + 1) / Tile.TILE_WIDTH, ty) ||
            collisionWithTile((int) (this.entityX() + bounds.x + bounds.width - 1) / Tile.TILE_WIDTH, ty)) {
            if (this.handler.getKeyManager().up) {
                this.yVector = -20;
            } else {
                this.yVector = 0;
            }
        }
        if (this.yVector < 20) {
            this.yVector++;
        }
    }

    public void positionPlayer(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void drawToMinimap(Graphics g) {
        g.setColor(Color.GREEN);
        g.fillOval((int) (((x / Tile.TILE_WIDTH) * 10) / 1.5 + handler.getWidth() - (int)((Tile.MINI_TILE_WIDTH * handler.getWorld().width())/1.2) - 15), (int) (((y / Tile.TILE_HEIGHT) * 10) / 1.5 + 17), 8, 8);
    }

    public void drawDashCooldown(Graphics g) {
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(20, 63, ((int) (DASH_COOLDOWN / 10) - (int) (dashTimer / 10)), 5);
    }

    public void drawHpBar(Graphics g) {
        g.setColor(Color.GRAY);
        g.fillRect(20, 20, 308, 38);
        g.setColor(Color.BLACK);
        g.fillRect(24, 24, 300, 30);
        g.setColor(Color.RED);
        g.fillRect(24, 24, healthPoints * 3, 30);
        g.setColor(Color.WHITE);
        g.setFont(new Font("TimesRoman", Font.PLAIN, 24));
        g.drawString(healthPoints + " / 100", 120, 48);
    }

    public void drawHitBox(Graphics g) {
        g.setColor(Color.RED);
        g.fillRect((int) (this.hitBox.x - handler.getGameCamera().getOffsetX()), (int) (this.hitBox.y - handler.getGameCamera().getOffsetY()), 30, 30);
    }

    private BufferedImage getCurrentAnimationFrame() {
        if (stagered) {
            if (this.directionRight) {
                return this.stageredRightAnimation.getCurrentFrame();
            } else {
                return this.stageredLeftAnimation.getCurrentFrame();
            }
        } else {
            if (this.attacking) {
                if (this.directionRight) {
                    return this.attackingRightAnimation.getCurrentFrame();
                } else {
                    return this.attackingLeftAnimation.getCurrentFrame();
                }
            } else if (dashing) {
                if (directionRight) {
                    return this.dashingRightAnimation.getCurrentFrame();
                } else {
                    return this.dashingLeftAnimation.getCurrentFrame();
                }
            } else if (this.xVector() < 0) {
                return this.walkingLeftAnimation.getCurrentFrame();
            } else if (this.xVector() > 0) {
                return this.walkingRightAnimation.getCurrentFrame();
            } else if (this.directionRight) {
                return this.lookingRightAnimation.getCurrentFrame();
            } else {
                return this.lookingLeftAnimation.getCurrentFrame();
            }
        }
    }
}
