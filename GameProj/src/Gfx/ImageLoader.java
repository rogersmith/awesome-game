package Gfx;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

public class ImageLoader {
    public static BufferedImage loadImage(String strPath){
        try {
            return ImageIO.read(new FileInputStream("C:\\Users\\u8690342\\IdeaProjects\\GameProj\\res\\textures\\" + strPath));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }
}
