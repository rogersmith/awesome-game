package Gfx;

import java.awt.image.BufferedImage;

public class Assets {

    private static final int PLAYER_WIDTH = 50;
    private static final int PLAYER_HEIGHT = 37;
    private static final int TILE_SIZE = 159;
    private static final int SWORD_SIZE = 970;

    public static BufferedImage[] playerLookingRight;
    public static BufferedImage[] playerLookingLeft;
    public static BufferedImage[] playerWalkingRight;
    public static BufferedImage[] playerWalkingLeft;
    public static BufferedImage[] playerAttackingRight;
    public static BufferedImage[] playerAttackingLeft;
    public static BufferedImage[] playerDashingRight;
    public static BufferedImage[] playerDashingLeft;
    public static BufferedImage[] playerStageredRight;
    public static BufferedImage[] playerStageredLeft;
    public static BufferedImage[] enemyLookingRight;
    public static BufferedImage[] enemyLookingLeft;
    public static BufferedImage[] enemyWalkingRight;
    public static BufferedImage[] enemyWalkingLeft;
    public static BufferedImage[] enemyAttackingRight;
    public static BufferedImage[] enemyAttackingLeft;
    public static BufferedImage[] enemyDashingRight;
    public static BufferedImage[] enemyDashingLeft;
    public static BufferedImage[] enemyStageredRight;
    public static BufferedImage[] enemyStageredLeft;
    public static BufferedImage background;
    public static BufferedImage grass1;
    public static BufferedImage grass2;
    public static BufferedImage grass3;
    public static BufferedImage dirt1;
    public static BufferedImage dirt2;
    public static BufferedImage dirt3;
    public static BufferedImage sky;

    public static void init(){
        initPlayerAnimation();
        initEnemyAnimation();
        SpriteSheet sheet;

        sheet = new SpriteSheet(ImageLoader.loadImage("tiles.png"));
        grass1 = sheet.crop(3 * TILE_SIZE, 1 * TILE_SIZE + 1, TILE_SIZE, TILE_SIZE);
        grass2 = sheet.crop(4 * TILE_SIZE, 1 * TILE_SIZE + 1, TILE_SIZE, TILE_SIZE);
        grass3 = sheet.crop(5 * TILE_SIZE, 1 * TILE_SIZE + 1, TILE_SIZE, TILE_SIZE);
        dirt1 = sheet.crop(0 * TILE_SIZE, 0 * TILE_SIZE, TILE_SIZE, TILE_SIZE);
        dirt2 = sheet.crop(1 * TILE_SIZE, 0 * TILE_SIZE, TILE_SIZE, TILE_SIZE);
        dirt3 = sheet.crop(2 * TILE_SIZE, 0 * TILE_SIZE, TILE_SIZE, TILE_SIZE);

        sheet = new SpriteSheet(ImageLoader.loadImage("empty.png"));
        sky = sheet.crop(0, 0, TILE_SIZE, TILE_SIZE);

        sheet = new SpriteSheet(ImageLoader.loadImage("background.jpg"));
        background = sheet.crop(200, 300, 1000, 600);
    }

    private static void initPlayerAnimation() {
        SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("adventurer_right_Sheet.png"));
        playerLookingRight = new BufferedImage[4];
        playerWalkingRight = new BufferedImage[6];
        playerAttackingRight = new BufferedImage[4];
        playerStageredRight = new BufferedImage[2];
        playerDashingRight = new BufferedImage[2];

        for(int i = 0; i < 4; i++){
            playerLookingRight[i] = sheet.crop(i * PLAYER_WIDTH,0, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 1; i < 7; i++){
            playerWalkingRight[i - 1] = sheet.crop(i * PLAYER_WIDTH, PLAYER_HEIGHT * 1, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 0; i < 4; i++){
            playerAttackingRight[i] = sheet.crop(i * PLAYER_WIDTH,PLAYER_HEIGHT * 7, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 1; i < 3; i++){
            playerStageredRight[i - 1] = sheet.crop(i * PLAYER_WIDTH,PLAYER_HEIGHT * 3, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 0; i < 2; i++){
            playerDashingRight[i] = sheet.crop(i * PLAYER_WIDTH,PLAYER_HEIGHT * 11, PLAYER_WIDTH, PLAYER_HEIGHT);
        }

        sheet = new SpriteSheet(ImageLoader.loadImage("adventurer_left_Sheet.png"));
        playerLookingLeft = new BufferedImage[4];
        playerWalkingLeft = new BufferedImage[6];
        playerAttackingLeft = new BufferedImage[4];
        playerStageredLeft = new BufferedImage[2];
        playerDashingLeft = new BufferedImage[2];

        for(int i = 6; i > 2; i--){
            playerLookingLeft[i - 3] = sheet.crop(i * PLAYER_WIDTH + 35, 0, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 5; i >= 0; i--){
            playerWalkingLeft[i] = sheet.crop(i * PLAYER_WIDTH + 35, PLAYER_HEIGHT * 1, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 6; i > 2; i--){
            playerAttackingLeft[3 - (i - 3)] = sheet.crop(i * PLAYER_WIDTH + 35, PLAYER_HEIGHT * 7, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 5; i > 3; i--){
            playerStageredLeft[5 - i] = sheet.crop(i * PLAYER_WIDTH + 35, PLAYER_HEIGHT * 3, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 6; i > 4; i--){
            playerDashingLeft[i - 5] = sheet.crop(i * PLAYER_WIDTH + 35,PLAYER_HEIGHT * 11, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
    }

    private static void initEnemyAnimation() {
        SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("enemy_right_Sheet.png"));
        enemyLookingRight = new BufferedImage[4];
        enemyWalkingRight = new BufferedImage[6];
        enemyAttackingRight = new BufferedImage[4];
        enemyStageredRight = new BufferedImage[2];
        enemyDashingRight = new BufferedImage[2];

        for(int i = 0; i < 4; i++){
            enemyLookingRight[i] = sheet.crop(i * PLAYER_WIDTH,0, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 1; i < 7; i++){
            enemyWalkingRight[i - 1] = sheet.crop(i * PLAYER_WIDTH, PLAYER_HEIGHT * 1, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 0; i < 4; i++){
            enemyAttackingRight[i] = sheet.crop(i * PLAYER_WIDTH,PLAYER_HEIGHT * 7, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 1; i < 3; i++){
            enemyStageredRight[i - 1] = sheet.crop(i * PLAYER_WIDTH,PLAYER_HEIGHT * 3, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 0; i < 2; i++){
            enemyDashingRight[i] = sheet.crop(i * PLAYER_WIDTH,PLAYER_HEIGHT * 11, PLAYER_WIDTH, PLAYER_HEIGHT);
        }

        sheet = new SpriteSheet(ImageLoader.loadImage("enemy_left_Sheet.png"));
        enemyLookingLeft = new BufferedImage[4];
        enemyWalkingLeft = new BufferedImage[6];
        enemyAttackingLeft = new BufferedImage[4];
        enemyStageredLeft = new BufferedImage[2];
        enemyDashingLeft = new BufferedImage[2];

        for(int i = 6; i > 2; i--){
            enemyLookingLeft[i - 3] = sheet.crop(i * PLAYER_WIDTH + 35, 0, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 5; i >= 0; i--){
            enemyWalkingLeft[i] = sheet.crop(i * PLAYER_WIDTH + 35, PLAYER_HEIGHT * 1, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 6; i > 2; i--){
            enemyAttackingLeft[3 - (i - 3)] = sheet.crop(i * PLAYER_WIDTH + 35, PLAYER_HEIGHT * 7, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 5; i > 3; i--){
            enemyStageredLeft[5 - i] = sheet.crop(i * PLAYER_WIDTH + 35, PLAYER_HEIGHT * 3, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
        for(int i = 6; i > 4; i--){
            enemyDashingLeft[i - 5] = sheet.crop(i * PLAYER_WIDTH + 35,PLAYER_HEIGHT * 11, PLAYER_WIDTH, PLAYER_HEIGHT);
        }
    }
}
