package Gfx;

import javax.swing.*;
import java.awt.*;

public class Display {
    private JFrame jFrame;
    private Canvas canvas;
    private String title;
    private int width;
    private int height;

    public Display(String title, int width, int height){
        this.title = title;
        this.height = height;
        this.width = width;

        this.createDisplay();
    }

    public JFrame jFrame(){
        return (this.jFrame);
    }

    public Canvas canvas(){
        return (this.canvas);
    }

    public int width(){
        return (this.width);
    }

    public int height(){
        return (this.height);
    }

    private void createDisplay(){
        this.jFrame = new JFrame(this.title);
        this.jFrame.setSize(this.width(), this.height());
        this.jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.jFrame.setResizable(false);
        this.jFrame.setLocationRelativeTo(null);
        this.jFrame.setVisible(true);

        this.canvas = new Canvas();
        this.canvas.setPreferredSize(new Dimension(this.width(), this.height()));
        this.canvas.setMaximumSize(new Dimension(this.width(), this.height()));
        this.canvas.setMinimumSize(new Dimension(this.width(), this.height()));
        this.canvas.setFocusable(false);

        this.jFrame.add(this.canvas);
        this.jFrame.pack();
    }
}
