package Tiles;

import Gfx.Assets;

public class DirtTile1 extends Tile {
    public DirtTile1(int tileID) {
        super(Assets.dirt1, tileID);
    }

    @Override
    public boolean isSolid(){
        return true;
    }
}
