package Tiles;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Tile {

    // Const Members
    public static final int TILE_WIDTH = 54;
    public static final int TILE_HEIGHT = 54;
    public static final int MINI_TILE_WIDTH = 8;
    public static final int MINI_TILE_HEIGHT = 8;

    // Data Members
    public static Tile[] tiles = new Tile[256];
    public static Tile grass1 = new GrassTile1(1);
    public static Tile grass2 = new GrassTile2(2);
    public static Tile grass3 = new GrassTile3(3);
    public static Tile dirt1 = new DirtTile1(4);
    public static Tile dirt2 = new DirtTile2(5);
    public static Tile dirt3 = new DirtTile3(6);
    public static Tile sky = new SkyTile(0);

    protected final int tileID;
    protected BufferedImage texture;

    public Tile(BufferedImage texture, int tileID){
        this.texture = texture;
        this.tileID = tileID;
        tiles[this.tileID] = this;
    }

    public void tick(){}

    public void render(Graphics g, int x, int y){
        g.drawImage(this.texture, x, y, TILE_WIDTH, TILE_HEIGHT, null);
    }

    public void miniMapRender(Graphics g, int x, int y, int miniMapX){
        g.drawImage(this.texture, (int)(((x/Tile.TILE_WIDTH)*10)/1.5 + miniMapX), (int)(((y/Tile.TILE_HEIGHT)*10)/1.5 + 15), MINI_TILE_WIDTH, MINI_TILE_HEIGHT, null);
    }

    public boolean isSolid(){
        return false;
    }

    public int getId(){
        return this.tileID;
    }
}
