package Tiles;

import Gfx.Assets;

public class GrassTile3 extends Tile{
    public GrassTile3(int tileID) {
        super(Assets.grass3, tileID);
    }

    @Override
    public boolean isSolid(){
        return true;
    }
}
