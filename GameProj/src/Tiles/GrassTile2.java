package Tiles;

import Gfx.Assets;

public class GrassTile2 extends Tile{
    public GrassTile2(int tileID) {
        super(Assets.grass2, tileID);
    }

    @Override
    public boolean isSolid(){
        return true;
    }
}
