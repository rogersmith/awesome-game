package Tiles;

import Gfx.Assets;

public class GrassTile1 extends Tile {
    public GrassTile1(int tileID) {
        super(Assets.grass1, tileID);
    }

    @Override
    public boolean isSolid(){
        return true;
    }
}
