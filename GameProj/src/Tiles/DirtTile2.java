package Tiles;

import Gfx.Assets;

public class DirtTile2 extends Tile {
    public DirtTile2(int tileID) {
        super(Assets.dirt2, tileID);
    }

    @Override
    public boolean isSolid(){
        return true;
    }
}
