package Tiles;

import Gfx.Assets;

public class DirtTile3 extends Tile {
    public DirtTile3(int tileID) {
        super(Assets.dirt3, tileID);
    }

    @Override
    public boolean isSolid(){
        return true;
    }
}
