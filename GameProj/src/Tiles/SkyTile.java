package Tiles;

import Gfx.Assets;

public class SkyTile extends Tile {
    public SkyTile( int tileID) {
        super(Assets.sky, tileID);
    }
}
